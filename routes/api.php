<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'Auth\LoginApiController@login');
Route::post('logout', 'Auth\LoginApiController@logout');
Route::post('register', 'Auth\RegisterApiController@register');
Route::group(['middleware' => 'auth:api'], function() {
    Route::get('transactions', 'TransactionsController@index');
    Route::get('transactions/{transactions}', 'TransactionsController@show');
    Route::post('transactions', 'TransactionsController@store');
    Route::put('transactions/{transactions}', 'TransactionsController@update');
    Route::delete('transactions/{transactions}', 'TransactionsController@delete');
});