mkdir crewber && cd crewber

git clone https://github.com/Laradock/laradock.git

##edit laradock/nginx/sites/default.conf

###from
root /var/www/public;

###to
root /var/www/crewber.com.au/public;

##Fire up Docker
docker-compose up -d nginx mysql redis workspace

##Enter workspace (/var/www)
docker-compose exec --user=laradock workspace  bash

##clone laravel-api-user from git into crewber.com.au
git clone git@bitbucket.org:oliver_bode/laravel-api-user.git crewber.com.au

##run composer
cd crewber.com.au
composer install

##populate database
php artisan migrate

##Web
http://localhost

##API

###Register
curl -X POST http://localhost/api/register -H "Accept: application/json" -H "Content-Type: application/json" -H "X-Requested-With: XMLHttpRequest" -d '{"name": "Your Name", "email": "name@email.com", "password": "secret", "password_confirmation": "secret"}'

###Login
curl -X POST http://localhost/api/login -H "Accept: application/json" -H "X-Requested-With: XMLHttpRequest" -d "email=name@email.com&password=secret"

###Logout
curl -X POST localhost/api/logout -H "Accept: application/json" -H "Content-type: application/json" -H "X-Requested-With: XMLHttpRequest" 

###You must use the token to get custom things from the db

####All Todos 
curl -X GET localhost/api/todo -H "Accept: application/json" -H "Content-type: application/json" -H "X-Requested-With: XMLHttpRequest"  -H "Authorization: Bearer random_token_from_login" 

####One Todo 
curl -X GET localhost/api/todo/1 -H "Accept: application/json" -H "Content-type: application/json" -H "X-Requested-With: XMLHttpRequest"  -H "Authorization: Bearer random_token_from_login" 

####Add Todo
curl -X POST localhost/api/todo -H "Accept: application/json" -H "X-Requested-With: XMLHttpRequest" -H "Authorization: Bearer random_token_from_login" -d "todo=I do not know what to do"

####Update Todo
curl -X PUT http://localhost/api/todo/1 -H "Authorization: Bearer random_token_from_login" -d "todo=something else to do"


##Database
docker-compose exec mysql  bash
mysql -u homestead -psecret homestead